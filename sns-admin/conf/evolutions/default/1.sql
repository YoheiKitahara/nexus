# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table member (
  id                        bigint auto_increment not null,
  name                      varchar(255),
  nick_name                 varchar(255),
  email1                    varchar(255),
  carrier1type              integer,
  email2                    varchar(255),
  carrier2type              integer,
  email3                    varchar(255),
  carrier3type              integer,
  tel                       varchar(255),
  status                    integer,
  sub_status                integer,
  ope_status                integer,
  pay_status                integer,
  point_status              integer,
  channel_id                integer,
  campaign_id               integer,
  message_count             integer,
  last_send_date            datetime,
  total_point               integer,
  last_bill_date            datetime,
  spend_point               integer,
  first_bill_date           datetime,
  user_agent                varchar(255),
  last_user_agent           varchar(255),
  identifier                varchar(255),
  last_identifer            varchar(255),
  ip                        varchar(255),
  prefecture_id             integer,
  blood_type                integer,
  gender                    integer,
  age                       integer,
  zodiac_type               integer,
  mind_type                 integer,
  style_type                integer,
  like_mind_type            integer,
  like_style_type           integer,
  like_age_type             integer,
  purpose_type              integer,
  action_type               integer,
  economic_type             integer,
  hot_type                  integer,
  shape_type                integer,
  pr                        varchar(255),
  operator_description      varchar(255),
  last_login_date           datetime,
  create_date               datetime not null,
  update_date               datetime not null,
  constraint pk_member primary key (id))
;

create table member_status (
  id                        bigint auto_increment not null,
  status                    integer,
  label                     varchar(255),
  create_date               datetime not null,
  update_date               datetime not null,
  constraint pk_member_status primary key (id))
;

create table member_sub_status (
  id                        bigint auto_increment not null,
  status                    integer,
  label                     varchar(255),
  create_date               datetime not null,
  update_date               datetime not null,
  constraint pk_member_sub_status primary key (id))
;




# --- !Downs

SET FOREIGN_KEY_CHECKS=0;

drop table member;

drop table member_status;

drop table member_sub_status;

SET FOREIGN_KEY_CHECKS=1;

