name := "sns-admin"

version := "1.0-SNAPSHOT"

libraryDependencies ++= Seq(
  javaJdbc,
  javaEbean,
  "mysql" % "mysql-connector-java" % "5.1.26",
  "commons-lang" % "commons-lang" % "2.6",
  "org.avaje.ebeanorm" % "avaje-ebeanorm-api" % "3.1.1",
  cache
)     

play.Project.playJavaSettings
