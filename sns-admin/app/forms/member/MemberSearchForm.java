package forms.member;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import database.entities.MemberStatus;
import database.entities.MemberSubStatus;
import play.data.validation.Constraints.EmailValidator;



public class MemberSearchForm {
	
	/** 検索件数*/
	public String limit = "100";
	
	/** Id */
	public String ids;
	public Map<String,String> idsOptions = new HashMap<String,String>(1);
	
	/**　電話番号*/
    public String tel;
    public Map<String,String> telOptions = new HashMap<String,String>(2);
    
    /** メインアドレス */
    public String email1;
    public Map<String,String> email1Options = new HashMap<String,String>(3);
	
    /** サブアドレス */
    public String email2;
    public Map<String,String> email2Options = new HashMap<String,String>(4);
	
    /**メインアドレスキャリア*/
    public List<String> career1Type = new ArrayList<String>(5);
    /**サブアドレスキャリア*/
    public List<String> career2Type = new ArrayList<String>(5);
	
    /**　会員ステータス*/
    public List<String> status = new ArrayList<String>();
    public Map<String,String> statusOptions = new HashMap<String,String>(2);

    /**　サブステータス*/
    public List<String> subStatus = new ArrayList<String>();
    public Map<String,String> subStatusOptions = new HashMap<String,String>(2);
    
    /**　料金ステータス*/
    public List<String> payStatus = new ArrayList<String>();
    public Map<String,String> payStatusOptions = new HashMap<String,String>(2);
    /**　ポイントステータス*/
    public List<String> pointStatus = new ArrayList<String>();
    public Map<String,String> pointStatusOptions = new HashMap<String,String>(2);
    /** 代理店Id */
    public List<String> channelId = new ArrayList<String>();
    public Map<String,String> channelIdoptions = new HashMap<String,String>(2);
    
    /** キャンペーンID */
    public String campaignId;
    
    
	/** 名前　まぁ本名じゃないよね*/
    public String name;
    /** ニックネーム*/
    public String nickName;

    
    
    /** 初期登録アドレス*/
    public String email3;
    /**　初回登録アドレスキャリア　タイプ*/
    public List<String> career3Type  = new ArrayList<String>(5);
    
    /** サブステータス(オペ)*/
    public List<String> opeStatus = new ArrayList<String>();
    
    
    /** 累計送信数 */
    public String messageCount;
    /** 最終送信日時*/
    public String lastSendDate;
    /** 累計購入ポイント*/
    public String totalPoint;
    /** 最終購入日時*/
    public String lastBillDate;
    /** 累計消費ポイント*/
    public String spendPoint;
    /** 初回購入日時*/
    public String firstBillDate;
    /** 登録時UA*/
    public String userAgent;
    /** 最終UA*/
    public String lastUserAgent;
    /** 登録時モバイル端末ID*/
    public String identifier;
    /** 最終モバイル端末ID*/
    public String lastIdentifer;
    /** IPアドレス*/
    public String ip;
    
    /**　都道府県*/
    public String prefectureId;
    /**　血液型*/
    public String bloodType;
    /** 性別*/
    public String gender;
    /**　年齢*/
    public String age;
    /**　星座*/
    public String zodiacType;
    /**性格*/
    public String mindType;
    /**容姿*/
    public String styleType;
    
    /**好みの性格*/
    public String likeMindType;
    /**好みの容姿*/
    public String likeStyleType;
    /**好みの年齢層*/
    public String likeAgeType;
    /** 目的*/
    public String purposeType;
    /**　行動力*/
    public String actionType;
    /**　経済力*/
    public String economicType;
    /** エロさ*/
    public String hotType;
    /** ルックス*/
    public String shapeType;
    /** PR */
    public String pr;
    /** オペメモ*/
    public String operatorDescription;
    
    /**最終ログイン日時*/
    public String lastLoginDate;
    /**作成日時＝初回登録日時*/
    public String createDate;
    /**更新日時*/
    public String updateDate;
    
    
    public List<MemberStatus> sutatusPulldown = MemberStatus.finder.all();
    
    public List<MemberSubStatus> subStatusPulldown = MemberSubStatus.finder.all();
    
    /**
     * validator
     * @return
     */
    public String validate(){
    	
    	//EmailValidator emailValidator = new EmailValidator();
    	//emailValidator.isValid(this.email1);
    	
    	return null;
    }

}
