package controllers;

import java.util.List;

import org.springframework.beans.BeanUtils;

import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import services.MemberService;
import views.html.member.index;
import views.html.member.list;
import database.entities.Member;
import forms.member.MemberSearchForm;



/**
 * 会員情報
 * @author k
 *
 */
public class MemberController extends Controller {
  
    /**
     * 会員管理画面機能一覧表示
     * @return
     */
	public static Result index() {
        //return ok("this is member");
		
		Form<MemberSearchForm> form = new play.data.Form<MemberSearchForm>(MemberSearchForm.class);
		
		MemberSearchForm data = new MemberSearchForm();
		//data.ids = "12345678";
		
		
		form = form.fill(data);
		
		
		//Map<String,String> anyData = new HashMap<String, String>();
		//anyData.put("ids", "12345");
		
		//MemberSearchForm formData = form.bind(anyData).get();
		
		

        return ok(index.render(form));
        
    }
    
    /**
     * 会員情報の登録
     * @return
     */
    public static Result add(){
    	return TODO;
    }
    
    /**
     * 会員検索
     * @return
     */
    public static Result search(){
    	
    	Form<MemberSearchForm> form = new play.data.Form<MemberSearchForm>(MemberSearchForm.class);
    	
    	MemberSearchForm memberForm = form.bindFromRequest().get();
    	
    	MemberService memberService = new MemberService();
    	
    	List<Member> members = memberService.search(memberForm);
    	
    	System.out.println(members.get(0));
    	
    	System.out.println("検索結果は　" + members.size());
    	
    	return ok(list.render(members));
    }
    
    public static Result test(){
    	return TODO;
    }
    
      
}
