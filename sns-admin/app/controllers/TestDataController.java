package controllers;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import constants.enums.Carrier;

import database.entities.Member;
import database.entities.MemberStatus;
import database.entities.MemberSubStatus;

import play.*;
import play.mvc.*;
import play.api.libs.json.Json;
import play.data.*;
import play.db.ebean.Model.Finder;
import scala.util.Random;

import views.html.member.*;


/**
 * 会員情報
 * @author k
 *
 */
public class TestDataController extends Controller {
  
    /**
     * 会員管理画面機能一覧表示
     * @return
     */
	public static Result generateMember() {
		
		DateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS");
		
		
		System.out.println("データ作成開始" + df.format(new java.util.Date(System.currentTimeMillis())));
		genMemberStatusData();
		genMemberSubStatusData();
		genRandomMemberData();
		System.out.println("データ作成終了" + df.format(new java.util.Date(System.currentTimeMillis())));
		return ok();
        
    }
 
	public static void genMemberStatusData(){
		
		
		MemberStatus entity = new MemberStatus();
		entity.status = 1;
		entity.label = "未認証";
		entity.save();
		
		entity = new MemberStatus();
		entity.status = 2;
		entity.label = "アドレス不良会員";
		entity.save();
		
		entity = new MemberStatus();
		entity.status = 3;
		entity.label = "仮退会会員";
		entity.save();
		
		entity = new MemberStatus();
		entity.status = 4;
		entity.label = "退会会員";
		entity.save();
		
		entity = new MemberStatus();
		entity.status = 5;
		entity.label = "不良会員";
		entity.save();
		
		entity = new MemberStatus();
		entity.status = 6;
		entity.label = "アクティブ";
		entity.save();
		
		entity = new MemberStatus();
		entity.status = 7;
		entity.label = "テストユーザー";
		entity.save();
	}
	
	public static void genMemberSubStatusData(){
		MemberSubStatus entity = new MemberSubStatus();
		entity.status = -2;
		entity.label = "強制無料会員";
		entity.save();
		
		entity = new MemberSubStatus();
		entity.status = -1;
		entity.label = "無料会員";
		entity.save();
		
		entity = new MemberSubStatus();
		entity.status = 1;
		entity.label = "通常会員";
		entity.save();
		
		entity = new MemberSubStatus();
		entity.status = 2;
		entity.label = "未払会員";
		entity.save();
		
		entity = new MemberSubStatus();
		entity.status = 3;
		entity.label = "購入一回";
		entity.save();
		
		entity = new MemberSubStatus();
		entity.status = 4;
		entity.label = "通常購入者";
		entity.save();
	}
        
    public static long genRandomMemberData(){
    	
    	Random rand = new Random();
    	Carrier[] career = Carrier.values();
    	
    	long count = 0;
    	for( ; count <= 500000 ; count++){
    		
    		Member mem = new Member();
    		mem.name = getFamilyName() + " " + getFirstName();
    		mem.nickName ="";
    		mem.gender = rand.nextInt(1);
    		Carrier c = career[rand.nextInt(career.length-1)];
    		mem.email1 = "SampleMainMail" + count+ "@" + c.getDomain();
    		mem.carrier1Type = c.getValue();
    		
    		c = career[rand.nextInt(career.length-1)];
    		mem.email2 = "SampleSubMail" + count + "@" + c.getDomain();
    		mem.carrier2Type = c.getValue();
    		c = career[rand.nextInt(career.length-1)];
    		
    		mem.email2 = "SampleRegisterMail" + count + "@" + c.getDomain();
    		mem.carrier2Type = c.getValue();
    		
    		mem.tel = ""+ rand.nextInt(9)+ rand.nextInt(9)+ rand.nextInt(9)+ rand.nextInt(9)+ rand.nextInt(9)+ rand.nextInt(9)+ rand.nextInt(9)+ rand.nextInt(9)+ rand.nextInt(9)+ rand.nextInt(9)+ rand.nextInt(9);
    		mem.status = rand.nextInt(7);
    		mem.subStatus = rand.nextInt(4);
    		mem.opeStatus = rand.nextInt(10);
    		mem.payStatus = rand.nextInt(10);
    		mem.pointStatus = rand.nextInt(10);
    		mem.channelId = rand.nextInt(10);
    	    mem.campaignId = rand.nextInt(10);
    	    mem.messageCount = rand.nextInt(1000);
    	    mem.lastSendDate = new Date();
    	    mem.totalPoint = rand.nextInt(100000);
    	    mem.lastBillDate = new Date();
    	    if(!mem.totalPoint.equals(0)){
    	    	mem.spendPoint = rand.nextInt(mem.totalPoint);
    	    }else{
    	    	mem.spendPoint = 0;
    	    }
    	    
    	    mem.firstBillDate = new Date();
    	    mem.userAgent = "TEST-USER-AGENT";
    	    mem.lastUserAgent = "TEST-LAST-USER-AGENT";
    	    mem.identifier = "TEST-IDENTIFIER";
    	    mem.lastIdentifer = "TEST-LAST-IDENTIFER";
    	    mem.ip = rand.nextInt(255) + "." + rand.nextInt(255) + "." + rand.nextInt(255) + "." + rand.nextInt(255);
    	    mem.prefectureId = rand.nextInt(47);
    	    mem.bloodType = rand.nextInt(4);
    	    mem.age = 18 + rand.nextInt(40);
    	    mem.zodiacType = rand.nextInt(12);
    	    mem.mindType = rand.nextInt(10);
    	    mem.styleType = rand.nextInt(10);
    	    
    	    mem.likeMindType = rand.nextInt(5);
    	    mem.likeStyleType = rand.nextInt(5);
    	    mem.likeAgeType = rand.nextInt(5);
    	    mem.purposeType = rand.nextInt(5);
    	    mem.actionType = rand.nextInt(5);
    	    mem.economicType = rand.nextInt(5);
    	    mem.hotType = rand.nextInt(5);
    	    mem.shapeType = rand.nextInt(5);
    	    mem.pr = "よろしくおねがいします！";
    	    mem.operatorDescription = "めもめもめもめもめもめもめも";
    	    mem.lastLoginDate = new Date();
    	    
    		mem.save();
    		
    	}
    	
    	return count;
    }
    
    public static Integer getStatusNumber(int n){

    	Random rand = new Random();
    	
    	return rand.nextInt(n);
    }
    
    /**
     * 名字をランダムに返す
     * @return
     */
    public static String getFamilyName(){
    	List<String> list = new ArrayList<String>();
    	
    	list.add("田中");
    	list.add("鈴木");
    	list.add("木村");
    	list.add("伊藤");
    	list.add("菊池");
    	list.add("安藤");
    	list.add("中村");
    	list.add("木下");
    	list.add("原");
    	list.add("原沢");
    	list.add("沼澤");
    	list.add("宮永");
    	list.add("宮本");
    	list.add("斉藤");
    	list.add("山田");
    	list.add("山本");
    	list.add("山浦");
    	list.add("三浦");
    	list.add("杉下");
    	list.add("杉本");
    	list.add("堂本");
    	list.add("堂島");
    	list.add("岡島");
    	list.add("長島");
    	list.add("長田");
    	list.add("田代");
    	list.add("代田");
    	list.add("大文字");
    	list.add("大岩");
    	list.add("岩崎");
    	list.add("篠崎");
    	list.add("篠田");
    	list.add("松井");
    	list.add("松田");
    	list.add("松本");
    	list.add("松下");
    	list.add("松野");
    	list.add("松里");
    	list.add("松原");
    	list.add("松川");
    	list.add("梅田");
    	list.add("梅本");
    	list.add("梅島");
    	list.add("梅木");
    	list.add("竹田");
    	list.add("竹本");
    	list.add("竹下");
    	list.add("竹沢");
    	list.add("北里");
    	list.add("北島");
    	list.add("北見");
    	list.add("北田");
    	Random rand = new Random();
    	
    	return list.get(rand.nextInt(list.size()-1)); 
    	
    }
    
    /**
     * 名前をランダムに返す
     * @return
     */
    public static String getFirstName(){
    	List<String> list = new ArrayList<String>();
    	
    	list.add("一郎");
    	list.add("次郎");
    	list.add("三郎");
    	list.add("太郎");
    	list.add("大介");
    	list.add("宏");
    	list.add("智己");
    	
    	list.add("大介");
    	list.add("潤");
    	list.add("ひろゆき");
    	list.add("元");
    	list.add("総司");
    	list.add("宗治");
    	list.add("聡耳");
    	list.add("智己");
    	list.add("洋平");
    	list.add("洋司");
    	list.add("洋次");
    	list.add("順一");
    	list.add("順次");
    	list.add("淳二");
    	list.add("順子");
    	list.add("明子");
    	list.add("智子");
    	list.add("洋子");
    	list.add("裕子");
    	list.add("博子");
    	list.add("広子");
    	list.add("寛子");
    	list.add("晶子");
    	list.add("聡子");
    	list.add("章子");
    	list.add("亜希子");
    	list.add("陽子");
    	list.add("瑶子");
    	list.add("真子");
    	list.add("雅美");
    	list.add("雅子");
    	list.add("友美");
    	list.add("智子");
    	list.add("朋子");
    	list.add("友子");
    	list.add("典子");
    	list.add("法子");
    	Random rand = new Random();
    	
    	return list.get(rand.nextInt(list.size()-1)); 
    	
    }
    
    
    public static Carrier getMailDomain(Carrier[] carrier){    	
    	Random rand = new Random();
    	return carrier[rand.nextInt(carrier.length-1)];

    }
    
  
}
