package database.entities;
 
import java.util.Date;

import javax.persistence.Entity;

import com.avaje.ebean.validation.NotNull;
 
@Entity
public class Member extends BaseEntity {

	private static final long serialVersionUID = 3935814349616916504L;


	/** 名前　まぁ本名じゃないよね*/
    @NotNull
    public String name;
    /** ニックネーム*/
    public String nickName;
    /** メインアドレス */
    @NotNull
    public String email1;
    /**メインアドレスキャリア　タイプ*/
    public Integer carrier1Type;
    /** サブアドレス */
    public String email2;
    /**サブアドレスキャリア　タイプ*/
    public Integer carrier2Type;
    /** 初期登録アドレス*/
    public String email3;
    /**　初回登録アドレスキャリア　タイプ*/
    public Integer carrier3Type;
    /**　電話番号*/
    public String tel;
    /**　会員ステータス*/
    public Integer status;
    
    /**　サブステータス*/
    public Integer subStatus;
    /** サブステータス(オペ)*/
    public Integer opeStatus;
    /**　料金ステータス*/
    public Integer payStatus;
    /**　ポイントステータス*/
    public Integer pointStatus;
    /** 代理店Id */
    public Integer channelId;
    /** キャンペーンID */
    public Integer campaignId;
    /** 累計送信数 */
    public Integer messageCount;
    /** 最終送信日時*/
    public Date lastSendDate;
    /** 累計購入ポイント*/
    public Integer totalPoint;
    /** 最終購入日時*/
    public Date lastBillDate;
    /** 累計消費ポイント*/
    public Integer spendPoint;
    /** 初回購入日時*/
    public Date firstBillDate;
    /** 登録時UA*/
    public String userAgent;
    /** 最終UA*/
    public String lastUserAgent;
    /** 登録時モバイル端末ID*/
    public String identifier;
    /** 最終モバイル端末ID*/
    public String lastIdentifer;
    /** IPアドレス*/
    public String ip;
    
    /**　都道府県*/
    public Integer prefectureId;
    /**　血液型*/
    public Integer bloodType;
    /** 性別*/
    public Integer gender;
    /**　年齢*/
    public Integer age;
    /**　星座*/
    public Integer zodiacType;
    /**性格*/
    public Integer mindType;
    /**容姿*/
    public Integer styleType;
    
    /**好みの性格*/
    public Integer likeMindType;
    /**好みの容姿*/
    public Integer likeStyleType;
    /**好みの年齢層*/
    public Integer likeAgeType;
    /** 目的*/
    public Integer purposeType;
    /**　行動力*/
    public Integer actionType;
    /**　経済力*/
    public Integer economicType;
    /** エロさ*/
    public Integer hotType;
    /** ルックス*/
    public Integer shapeType;
    /** PR */
    public String pr;
    /** オペメモ*/
    public String operatorDescription;
    
    /**最終ログイン日時*/
    public Date lastLoginDate;
    
    
    public static Finder<Long, Member> finder = new Finder<Long, Member>(Long.class, Member.class);
 

}