package database.entities;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.Date;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

import play.db.ebean.Model;

import com.avaje.ebean.annotation.CreatedTimestamp;

@MappedSuperclass

public class BaseEntity extends Model{
	private static final long serialVersionUID = -3053158547772184393L;

	
	@Id
    public Long id;
	
	/**作成日時*/
    @CreatedTimestamp
    public Date createDate;
    /**更新日時*/
    @Version
    public Date updateDate;
    
    public String toString(){
    	StringBuffer sb = new StringBuffer();
    	sb.append(this.getClass().getSimpleName() + ":\n");
    	try {
			for(Field field : this.getClass().getFields()){
				sb.append(field.getName());
				sb.append(" = ");
				
				Object fObj = field.get(this);
				if(fObj != null){
					if (fObj.getClass().isArray()) {
						sb.append("[");
						int length = Array.getLength(fObj);
						for(int idx=0 ; idx<length ; idx++){
							sb.append(Array.get(fObj, idx));
							if (idx < length-1) {
								sb.append(",");
							}
						}
						sb.append("]");
					}else{
						sb.append(fObj);
					}
				}else{
					sb.append("null");
				}
				
				sb.append("\n");
			}
			sb.append("\n");
		} catch (SecurityException | IllegalArgumentException
				| IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	return sb.toString();
    }

}
