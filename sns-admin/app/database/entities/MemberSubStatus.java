package database.entities;
 
import javax.persistence.Entity;
 
@Entity
public class MemberSubStatus extends BaseEntity {

	private static final long serialVersionUID = -2284132560358475866L;

	
	/** ステータスコード*/
	public int status;
	
	/** ラベル*/
	public String label;
	
    public static Finder<Long, MemberSubStatus> finder = new Finder<Long, MemberSubStatus>(Long.class, MemberSubStatus.class);
 
}