package database.entities;
 
import javax.persistence.Entity;
 
@Entity
public class MemberStatus extends BaseEntity {

	private static final long serialVersionUID = 6150521266451900811L;

	
	/** ステータスコード*/
	public int status;
	
	/** ラベル*/
	public String label;

    public static Finder<Long, MemberStatus> finder = new Finder<Long, MemberStatus>(Long.class, MemberStatus.class);
 
    
}