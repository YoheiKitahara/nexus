package services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import utils.NexusUtil;

import com.avaje.ebean.Expr;
import com.avaje.ebean.ExpressionList;
import com.avaje.ebean.Query;

import database.entities.Member;
import forms.member.MemberSearchForm;

/**
 * 会員情報に対するサービスクラス
 * @author k
 *
 */
public class MemberService extends BaseService {
	
	
	public List<Member> search(MemberSearchForm memberForm){
		
		Query<Member> query = Member.finder.setMaxRows(Integer.valueOf(memberForm.limit));
    	
    	ExpressionList<Member> where = query.where();
    	
    	
    	//System.out.println(memberForm.ids);
    	
    	
    	//会員ID
    	if(StringUtils.isNotBlank(memberForm.ids)){
    		String[] idArray = StringUtils.split(memberForm.ids.replaceAll("\r\n", "\n"), "\n");
    		if(memberForm.idsOptions.containsKey("not")){
    			where.not(Expr.in("id" ,Arrays.asList(idArray)));
    		}else{
    			where.in("id" ,Arrays.asList(idArray));
    		}
    	}

    	//////////////
    	//電話番号
    	//////////////
    	if(StringUtils.isNotBlank(memberForm.tel) && memberForm.telOptions.containsKey("enable")){
    		String[] telArray = StringUtils.split(memberForm.tel.replaceAll("\r\n", "\n"), "\n");
    		if(memberForm.telOptions.containsKey("not")){
    			where.not(Expr.in("tel" ,Arrays.asList(telArray)));
    		}else if(memberForm.telOptions.containsKey("fuzzy")){
    		//TODO
    		}else{
    			where.in("tel" ,Arrays.asList(telArray));
    		}

    	}

    	//////////////////
    	//メインメールアドレス
    	//////////////////
    	if(StringUtils.isNotBlank(memberForm.email1) && memberForm.email1Options.containsKey("enable")){
    		System.out.println(memberForm.email1);
    		System.out.println(memberForm.email1.replaceAll("\r\n", "\n"));
    		String[] telArray = StringUtils.split(memberForm.email1.replaceAll("\r\n", "\n"), "\n");
    		if(memberForm.email1Options.containsKey("not")){
    			//query.where().
    		}else if(memberForm.email1Options.containsKey("fuzzy")){
    			query.where().in("email1" ,Arrays.asList(telArray));
    		}else{
    			query.where().in("email1" ,Arrays.asList(telArray));
    		}
    		//TODO 否定時の処理
    	}
    	/////////////////////
    	//サブメールアドレス
    	/////////////////////
    	if(StringUtils.isNotBlank(memberForm.email2) && memberForm.email2Options.containsKey("enable")){
    		String[] telArray = StringUtils.split(memberForm.email2.replaceAll("\\r\\n", "\\n"), "\\n");
    		if(memberForm.email2Options.containsKey("not")){
    			//query.where().
    		}else if(memberForm.email2Options.containsKey("fuzzy")){
        		//TODO
    		}else if(memberForm.email2Options.containsKey("ng")){
        		//TODO
    		}else{
    			query.where().in("email2" ,Arrays.asList(telArray));
    		}
    			
    	}
    	//////////////////////
    	//メインメールキャリア
    	//////////////////////
    	List<String> career1TypeParam = NexusUtil.removeEmptyValue(memberForm.career1Type);
    	if(!career1TypeParam.isEmpty()){
    		
    		where.in("carrier1Type" , career1TypeParam);
        }
    	////////////////////
    	//サブメールキャリア
    	/////////////////////
    	List<String> career2TypeParam = NexusUtil.removeEmptyValue(memberForm.career2Type);
    	if(!career2TypeParam.isEmpty()){
    		
    		where.in("carrier2Type" , career2TypeParam);
        }
    	
    	////////////////
    	//ステータス
    	///////////////
    	List<String> statusParam = NexusUtil.removeEmptyValue(memberForm.status);
    	if(!statusParam.isEmpty() && memberForm.statusOptions.containsKey("enable")){
    		where.in("status" , statusParam);
        }
    	//////////////////
    	//サブステータス
    	//////////////////
    	List<String> subStatusParam = NexusUtil.removeEmptyValue(memberForm.subStatus);
    	if(!subStatusParam.isEmpty() && memberForm.subStatusOptions.containsKey("enable")){
    		where.in("subStatus" , subStatusParam);
        }
    	
		//////////////////
		//料金ステータス
		//////////////////
		List<String> payStatusParam = NexusUtil.removeEmptyValue(memberForm.payStatus);
		if(!payStatusParam.isEmpty() && memberForm.payStatusOptions.containsKey("enable")){
			where.in("payStatus" , payStatusParam);
		}
		//////////////////
		//ポイントステータス
		//////////////////
		List<String> pointStatusParam = NexusUtil.removeEmptyValue(memberForm.pointStatus);
		if(!pointStatusParam.isEmpty() && memberForm.pointStatusOptions.containsKey("enable")){
			where.in("pointStatus" , pointStatusParam);
		}
    	
    	if(StringUtils.isNotBlank(memberForm.gender)){
    		where.eq("gender", memberForm.gender);
    	}
    	
    	    	
    	System.out.println(query.toString());
    	System.out.println(query.findRowCount());
    	
    	return query.findList();

		
	}
	

}
