package constants.enums;

/**
 * 携帯種別
 * @author k
 *
 */
public enum Carrier implements IEnum{
	
	DOCOMO(1 , "DoCoMo" ,"docomo.ne.jp"),
	AU(2, "AU/Tu-Ka","au.ne.jp"),
	SOFTBANK(3 , "SoftBank/Vodafone","softbank.ne.jp"),
	WILLCOM(3 , "WILLCOM" ,"willcom"),
	PC(4 , "PC" ,"gmail.com"),
	;
	
	private int value;
	private String domain;
	private String label;
	
	private Carrier(int n , String label , String domain){
		this.domain = domain;
		this.value = n;
		this.label = label;
	}
	public String getDomain(){
		return this.domain;
	}
	public Integer getValue(){
		return this.value;
	}
	public String getLabel(){
		return this.label;
	}

}
