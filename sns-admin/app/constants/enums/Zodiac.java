package constants.enums;

/**
 * 星座
 * @author k
 *
 */
public enum Zodiac implements IEnum {

	OHITUJI(1,"牡羊座"),
	OUSI(2 ,"牡牛座"),
	FUTAGO(3 , "双子座"),
	KANI(4 , "蟹座"),
	SISI(5 , "獅子座"),
	OTOME(6 ,"乙女座"),
	TENBIN(7 , "天秤座"),
	SASORI(8 , "蠍座"),
	ITE(9 , "射手座"),
	YAGI(10 , "山羊座"),
	MIZUGAME(11 , "水瓶座"),
	UO(12 , "魚座"),
	;
	
	private Zodiac(int value , String label){
		this.value = value;
		this.label = label;
	}
	
	
	private String label;
	private int value;
	
	public Integer getValue(){
		return this.value;
	}
	public String getLabel(){
		return this.label;
	}
}
