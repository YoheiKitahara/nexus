package constants.enums;

/**
 * 血液型
 * @author k
 *
 */
public enum Blood implements IEnum {
	TYPE_A(1 , "A型"),
	TYPE_B(2 , "B型"),
	TYPE_O(3 , "O型"),
	TYPE_AB(4 , "AB型"),
	;
	
	private Blood(int value , String label){
		this.value = value;
		this.label = label;
	}
	
	
	private String label;
	private int value;
	
	public Integer getValue(){
		return this.value;
	}
	public String getLabel(){
		return this.label;
	}
}
