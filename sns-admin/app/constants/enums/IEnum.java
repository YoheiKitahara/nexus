package constants.enums;

public interface IEnum {

	/** ラベル */
	String getLabel();
	/** 値*/
	Integer getValue();
	
}
