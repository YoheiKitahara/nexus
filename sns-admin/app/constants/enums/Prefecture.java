package constants.enums;

/**
 * 都道府県
 * @author k
 *
 */
public enum Prefecture implements IEnum {
	
	HOKKAIDO(1 , "北海道"),
	//東北
	AOMORI(2 , "青森県"),
	IWATE(3 , "岩手県"),
	MIYAGI(4 , "宮城県"),
	AKITA(5 , "秋田県"),
	YAMAGATA(6 , "山形県"),
	FUKUSIMA(7 , "福島県"),
	
	//関東
	IBARAKI(8 , "茨城県"),
	TOTIGI(9 , "栃木県"),
	GUNMA(10 , "群馬県"),
	SAITAMA(11 , "埼玉県"),
	TIBA(12 , "千葉県"),
	TOKYO(13 , "東京都"),
	KANAGAWA(14 , "神奈川県"),
	
	//中部
	NIGATA(15 , "新潟県"),
	TOYAMA(16 , "富山県"),
	ISIKAWA(17 , "石川県"),
	FUKUI(18 , "福井県"),
	YAMANASI(19 , "山梨県"),
	NAGANO(20 , "名古屋県"),
	GIFU(21 , "岐阜県"),
	SIZUOKA(22 , "静岡県"),
	AITI(23 , "愛知県"),
	MIE(24 , "三重県"),
	
	//近畿
	SIGA(25,"滋賀県"),
	KYOTO(26,"京都府"),
	OSAKA(27,"大阪府"),
	HYOGO(28,"兵庫県"),
	NARA(29,"奈良県"),
	WAKAYAMA(30,"和歌山県"),
	
	//中国
	TOTTORI(31 , "鳥取県"),
	SIMANE(32 , "島根県"),
	OKAYAMA(33 , "岡山県"),
	HIROSIMA(34 , "広島県"),
	YAMAGUTI(35 , "山口県"),
	
	//四国
	TOKUSIMA(36 , "徳島県"),
	KAGAWA(37 , "香川県"),
	EHIME(38 , "愛媛県"),
	KOUTI(39 , "高知県"),
	
	//九州
	FUKUOKA(40 , "福岡県"),
	SAGA(41 , "佐賀県"),
	NAGASAKI(42 , "長崎県"),
	KUMAMOTO(43 , "熊本県"),
	OITA(44 , "大分県"),
	MIYAZAKI(45 , "宮崎県"),
	KAGOSIMA(46 , "鹿児島県"),
	//
	OKINAWA(47 , "沖縄県"),
	;
	
	private int value;
	private String label;
	
	private Prefecture(int value , String label){
		this.value = value;
		this.label = label;
	}
	public Integer getValue(){
		return this.value;
	}
	public String getLabel(){
		return this.label;
	}

}
