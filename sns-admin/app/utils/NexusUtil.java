package utils;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

/**
 * Nexus ユーティリティ
 * @author k
 *
 */
public class NexusUtil {
	
	/**
	 * リスト内から空のValueを取り除いたリストを返却
	 * @param List<String> list
	 * @return List<String> resultList
	 */
	public static List<String> removeEmptyValue(final List<String> list){
		
		List<String> resultList = new ArrayList<String>();
		
		for(String str : list){
			if(StringUtils.isNotEmpty(str.trim())){
				resultList.add(str.trim());
			}
		}
		return resultList;
		
	}

}
